# [Crypto Bokk Bot]  🤖
Chatbot that reads tweets and interprets market sentiment: bullish or bearish.


## Instructions
⚠️ You must have previously installed pipenv

👉 Tutorial to install pipenv

(Tutorial to install pipenv)[https://pipenv-es.readthedocs.io/es/latest/]

👉 Tutorial to raise the environment and a demo

(Tutorial donde muestro como se instala)[https://youtu.be/GkqfwGow_CI]


Step 1.
```
 git clone https://codeberg.org/erickbarcenas/crypto_bokk_bot
```

Step 2.
```
cd crypto_bokk_bot
```

Step 3.
```
pipenv install
```

Step 4.
```
pipenv shell
```

Step 5.

The file shared by another means called env must be renamed to .env

Step 6.
```
python main.py
```
Everything ok? or some mistake? 
If so, please send an email to barcenas.dev@gmail.com



