import re
import json
import requests
from functions.config import settings
from functions.helpers import save_tweets

bearer_token = settings.TWITTER_BEARER_TOKEN

def bearer_oauth(r):
    """
    Method required by bearer token authentication.
    """
    r.headers["Authorization"] = f"Bearer {bearer_token}"
    r.headers["User-Agent"] = "v2FilteredStreamPython"
    return r


def get_rules():
    response = requests.get(
        "https://api.twitter.com/2/tweets/search/stream/rules", auth=bearer_oauth
    )
    if response.status_code != 200:
        raise Exception(
            "Cannot get rules (HTTP {}): {}".format(response.status_code, response.text)
        )
    return response.json()


def delete_all_rules(rules):
    if rules is None or "data" not in rules:
        return None

    ids = list(map(lambda rule: rule["id"], rules["data"]))
    payload = {"delete": {"ids": ids}}
    response = requests.post(
        "https://api.twitter.com/2/tweets/search/stream/rules",
        auth=bearer_oauth,
        json=payload
    )
    if response.status_code != 200:
        raise Exception(
            "Cannot delete rules (HTTP {}): {}".format(
                response.status_code, response.text
            )
        )
    # print(json.dumps(response.json()))

def set_rules(delete, crypto, crypto_alias):
    # You can adjust the rules if needed
    sample_rules = [
        {"value": crypto, "tag": crypto},
        {"value": crypto_alias, "tag": crypto_alias}
    ]
    payload = {"add": sample_rules}
    response = requests.post(
        "https://api.twitter.com/2/tweets/search/stream/rules",
        auth=bearer_oauth,
        json=payload,
    )
    if response.status_code != 201:
        raise Exception(
            "Cannot add rules (HTTP {}): {}".format(response.status_code, response.text)
        )
    # print(json.dumps(response.json()))


def get_stream(set):
    result =[]
    raw_tweets = []
    max_count = settings.TWEETS_DOWNLOAD
    count =0
    print("Connecting to the twitter REST API")
    res = requests.get(
        "https://api.twitter.com/2/tweets/search/stream", 
        auth=bearer_oauth, stream=True,
    )
    if res.status_code != 200:
        raise Exception(
            "Cannot get stream (HTTP {}): {}".format(
                res.status_code, res.text
            )
        )
    print("Downloading tweets ...")
    for response_line in res.iter_lines():
        if response_line:
            result = json.loads(response_line)
            if result['data']['text'].startswith('RT @'):
                continue
            # print(json.dumps(json_response, indent=4, sort_keys=True))
            line = result['data']['text']
            raw_tweets.append(line)
            count = count + 1
        if count > max_count:
            tweets_clean = []
            for tweet in raw_tweets:

                tweet_clean = clean_tweet(tweet)
                if len(tweet_clean) > 0:
                    tweets_clean.append(tweet_clean)
            return tweets_clean
        if count%max_count == 0:
            print(f"collected {count} tweets!")

            
# To set your enviornment variables in your terminal run the following line:
# export 'BEARER_TOKEN'='<your_bearer_token>'

bearer_token = settings.TWITTER_BEARER_TOKEN


def get_new_tweets(path=None, crypto='bitcoin', crypto_alias='BTC'):
    rules = get_rules()
    delete = delete_all_rules(rules)
    set = set_rules(delete, crypto, crypto_alias)
    tweets = get_stream(set)

    if path is not None:
        save_tweets(path, tweets)
    
    return tweets


def clean_tweet(tweet):
    tweet = remove_emojis(tweet)
    tweet = settings.REPLACE_NO_SPACE.sub(settings.NO_SPACE, tweet.lower())
    tweet = settings.REPLACE_WITH_SPACE.sub(settings.SPACE, tweet)
    tweet = re.sub(settings.RE_TWEET, settings.NO_SPACE, tweet)
    tweet = re.sub(settings.HTTP, settings.NO_SPACE, tweet)
    tweet = re.findall(settings.LETTER, tweet)
    tweet = " ".join(tweet)
    return tweet


def remove_emojis(data):
    emoj = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642" 
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                      "]+", re.UNICODE)
    return re.sub(emoj, '', data)