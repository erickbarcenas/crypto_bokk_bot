import re
import csv
import numpy as np
from sklearn.metrics import accuracy_score

def get_accuracy(model, target, X_train, X_test):
    model.fit(X_train, target)
    accuracy = accuracy_score(target, model.predict(X_test))
    print (f" ===== Accuracy: {accuracy} =====")
    return accuracy

def save_weights(filename, accuracy):
    # Guarda los pesos del modelo en formato .npz
    try:
        np.savez(
            filename,
            accuracy=accuracy,
        )
        print(f'File {filename} successfully saved!')
        return True
    except Exception as e:
        print(f'An error occurred while saving the file: {e}!')
        return False



def save_tweets(filename, tweets):
    
    try:
        for tweet in tweets:
            with open(filename, 'a+', newline='') as write_obj:
                writer = csv.writer(write_obj)
                writer.writerow([tweet])
        write_obj.close()
        print('File saved successfully!')
    except Exception as e:
        print(f"An exception occurred: {e}")


    



