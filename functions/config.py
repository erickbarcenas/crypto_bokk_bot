import re
from decouple import config


class Settings:
    TWEETS_DOWNLOAD = 20

    TWITTER_API_KEY: str = config('TWITTER_API_KEY')
    TWITTER_API_KEY_SECRET: str = config('TWITTER_API_KEY_SECRET')
    TWITTER_BEARER_TOKEN: str = config('TWITTER_BEARER_TOKEN')

    # print(f"------------bearer {TWITTER_BEARER_TOKEN}---------------")
    CRYPTOS =  {
    "BTC": "Bitcoin",
    # "ETH": "Ethereum",
    # "DOT": "Polkadot",
    # "ADA": "Cardano"
    }

    # -----------------------> CONTEXTS ----------------------------- 

    CONTEXTS = {
        "CRYPTO": 0
    }

    # Callback data
    # -----------------------> ACTIONS -----------------------------
    ACTIONS = {}

    CRYPTO_MENU = list(CRYPTOS.keys())
    CRYPTO_FULL_NAME = list(CRYPTOS.values())

    # -----------------------> CONSTANTS -----------------------------

    REPLACE_NO_SPACE = re.compile("(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])|(\d+)")
    REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")
    NO_SPACE = ""
    SPACE = " "
    HTTP = 'http\S+'
    RE_TWEET = 'RT @[\w_]+: '
    LETTER = "[a-zA-Z\-\.'/]+"
    
    STOP_WORDS = ['in', 'of', 'at', 'a', 'the', 'if', 'but', 'we', 'he', 'she', 'they']
    
settings = Settings()

