# Functions that help for training the model

# 1. Download new tweets
# from functions.tweets import get_new_tweets
# get_new_tweets('./unlabeled_data/bitcoin_raw.csv', 'BTC', 'bitcoin')

# 2. Create new model for SVM
# from models.svm import svm
# svm('./data/bitcoin/test.csv', './data/bitcoin/train.csv', save_params=True)

# 3. Create new model for Naive Bayes
# from models.naive_bayes import naive_bayes
# naive_bayes('./data/bitcoin/test.csv', './data/bitcoin/train.csv', save_params=True)


