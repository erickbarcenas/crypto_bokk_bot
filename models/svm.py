import pickle
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from functions.helpers import get_accuracy
from functions.helpers import save_weights

def svm(path_tweets_train, path_tweets_test, save_params):

    tweets_train = pd.read_csv(path_tweets_train)
    tweets_test = pd.read_csv(path_tweets_test)

    X_train_target = tweets_train["value"]
    
    # Countvectorizer is a method to convert text to numerical data
    coun_vect = CountVectorizer(binary=True, ngram_range=(1, 2))

    coun_vect.fit(tweets_train)
    X_train = coun_vect.transform(tweets_train["tweet"])
    X_test = coun_vect.transform(tweets_test["tweet"])

    model = LinearSVC(C=0.01)
    model.fit(X_train, X_train_target)
    accuracy = get_accuracy(model, X_train_target, X_train, X_test)
    

    if save_params == True:
        pickle.dump(model, open("./models/model_svm.sav", 'wb'))
        pickle.dump(coun_vect, open("./models/vector_svm.sav", 'wb'))
        save_weights("./models/svm", accuracy)

    return model, coun_vect, accuracy


# https://scikit-learn.org/stable/model_persistence.html