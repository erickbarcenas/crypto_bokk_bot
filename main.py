#!/usr/bin/env python
# pylint: disable=C0116,W0613
# This program is dedicated to the public domain under the CC0 license.

"""
Final Project:
Intelligent Word Processing

Member:
Bárcenas Martínez Erick Iván
barcenas.dev@gmail.com

Description:
This is a Cryptocurrency Trading Bot

"""



# All necessary libraries are imported
# from random import randrange
# from joblib import dump, load
# from app.functions.stream_tweets import get_tweets
from email import message
import numpy as np
import logging
import pickle
from functions.tweets import get_new_tweets
from decouple import config
from functions.config import settings
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)



# ==========================================================
# ========================= SETUP ==========================
# ==========================================================

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

# Load of constants
TELEGRAM_API_TOKEN = config('TELEGRAM_API_TOKEN')

# ==========================================================
# ================ INITIALIZER MENU ========================
# ==========================================================
def start(update: Update, context: CallbackContext) -> int:
    """Starts the conversation and asks the user about their crypto."""
    reply_keyboard = [settings.CRYPTO_MENU]

    update.message.reply_text(
        "👋 Hi! My name is Bokk, I'm the teacher of the market. \nI will hold a conversation with you.\n\n"
        "Send /cancel to stop talking to me.\n\n"
        "What cryptocurrency are you interested in?",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder='BTC or ETH?'
        ),
    )
    return settings.CONTEXTS["CRYPTO"]

# ==========================================================
# =============== MENU TO CHOOSE A CRYPTOCURRENCY ==========
# ==========================================================
def crypto(update: Update, context: CallbackContext) -> int:
    """Shows the sentiment of a cryptocurrency"""
    # first_name = update.message.from_user.first_name
    text = update.message.text

    if text in settings.CRYPTO_MENU: 
        crypto = settings.CRYPTO_MENU[settings.CRYPTO_MENU.index(text)]
        
        result, accuracy, status = get_sentiment(crypto)

        if status:
            if result[0] == 1:
                message = f"📈 Bullish - (accuracy: { accuracy } %)"
            else:
                message = f"📉 Bearish - (accuracy: { accuracy } %)"
        else:
            message = "🔱 Coming soon!"

        update.message.reply_text(
            message +
            '\n\n /new',
            reply_markup=ReplyKeyboardRemove(),
        )
        return settings.CONTEXTS["CRYPTO"]
        
    else:
        update.message.reply_text(
            error = "Wrong message! Please select a valid cryptocurrency. Use /start" +
            ' /skip',
            reply_markup=ReplyKeyboardRemove(),
        )

# ==========================================================
# ============ MENU TO CHOOSE NEW CRYPTOCURRENCY ===========
# ==========================================================
def new_crypto(update: Update, context: CallbackContext) -> int:
    """Starts a new conversation about a new crypto selected"""
    
    update.message.reply_text(
        "Please choose a cryptocurrency of your interest",
        reply_markup=ReplyKeyboardMarkup(
            keyboard=[settings.CRYPTO_MENU], one_time_keyboard=True
        ),
    )

    return settings.CONTEXTS["CRYPTO"]


# ==========================================================
# =================== CANCEL FUNCTION ======================
# ==========================================================
def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    # user = update.message.from_user
    # logger.info(f"User { user.first_name } canceled the conversation.")
    update.message.reply_text(
        message='Bye! I hope you have a great trade!\nIn digital we trust🚀', 
        reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# ==========================================================
# ===================== MAIN FUNCTION ======================
# ==========================================================

def main() -> None:
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(f"{TELEGRAM_API_TOKEN}")

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the state(s) CRYPTO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            settings.CONTEXTS["CRYPTO"]: [
                    MessageHandler(
                    Filters.regex('^(BTC|ETH|DOT|ADA|Other)$'), crypto)
                ]
        },
        fallbacks=[
            CommandHandler(command='new', callback=new_crypto),
            CommandHandler('cancel', cancel)
            ],
    )
    dispatcher.add_handler(conv_handler)
    # Start the Bot
    updater.start_polling() 
    updater.idle()



# ==========================================================
# ================  SENTIMENT ANALYSIS =====================
# ==========================================================
def get_sentiment(crypto):
    # 1. get new tweets from the cripto currency
    test_tweets = get_new_tweets(path=None, crypto='bitcoin', crypto_alias='BTC')
    # 2. sentiment analysis algorithms are applied
    result, accuracy, status = get_prediction('svm', crypto, test_tweets)
    return result, accuracy, status


def get_prediction(model_name, crypto, test_tweets):
    status, model, vector, accuracy, result = get_weights(model_name, crypto)
    if status:
        result = model.predict(vector.transform(test_tweets))
    return result, accuracy, status

def get_weights(model_name, crypto):
    if model_name == 'svm':
        status = True
        if crypto == 'BTC':
            model = pickle.load(open("./models/model_svm.sav", 'rb'))
            vector = pickle.load(open("./models/vector_svm.sav", 'rb'))
            parameters = np.load("./models/svm.npz")
            accuracy = float(parameters['accuracy']) * 100
        else:
            model = None
            vector = None
            accuracy = 0
            status = False
    else: # naive_bayes
        status = True
        if crypto == 'BTC':
            model = pickle.load(open("./models/model_bayes.sav", 'rb'))
            vector = pickle.load(open("./models/vector_bayes.sav", 'rb'))
            parameters = np.load("./models/bayes.npz")
            accuracy = float(parameters['accuracy']) * 100
        else:
            model = None
            vector = None
            accuracy = 0
            status = False
            
    return status, model, vector, round(accuracy, 2), []

# ==========================================================
# ===================== ENTRY POINT ========================
# ==========================================================
if __name__ == '__main__':
    main()